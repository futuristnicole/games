import React from 'react';
import { Switch, Route } from 'react-router-dom'

import WallDetails from '../05-pages/WallWidget/WallDetails/WallDetails'


const WallWidget = () => (
    <div className=''>
    <Switch>
        <Route exact path="/addWall" component={WallDetails} />
    </Switch>
    </div>
);
export default WallWidget;