import styled from 'styled-components';
import {  BreakM } from '../../00-base/breakpoints';

export const FlexContainer = styled.div`
    display: flex;
    margin: 20px auto;
    max-width: 1140px;
    width: 90%;
    justify-content: center;
    align-items: center;
    padding: 2rem;
    flex-direction: column-reverse;
    @media screen and (min-width: ${BreakM}) {
        flex-direction: row;
      }
`;
export const FlexBox = styled.div`
    flex: 1;
    margin: 10px;
    padding: 20px;
    text-align: center;
`;