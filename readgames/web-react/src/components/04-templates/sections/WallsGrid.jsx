import styled from 'styled-components';
import { BreakS } from '../../00-base/breakpoints';


export const WallsGrid = styled.div`
    display: grid;
    grid-template-columns:  1fr;
    grid-gap: 20px;
    margin: auto;
    padding: 0; 
    // grid-auto-rows: minmax(200px, auto);
    @media screen and (min-width: ${BreakS}) {
        grid-template-columns: repeat(auto-fit, minmax(350px, 1fr));

        }
`;