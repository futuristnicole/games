import styled from 'styled-components';
import { BreakS, BreakM } from '../../00-base/breakpoints';


export const Grid3 = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: 20px;
    margin: auto;
    padding: 0; 
  
    @media screen and (min-width: ${BreakS}) {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-gap: 20px;
      margin: auto;
      padding: 0;
    }
    @media screen and (min-width: ${BreakM}) {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      grid-gap: 20px;
      margin: auto;
      padding: 0;;
    }
  
`;