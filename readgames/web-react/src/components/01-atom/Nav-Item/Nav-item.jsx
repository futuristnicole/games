import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

import { NavItem, NavItemli } from './Nav-item.styled';


class SideNavItem extends Component {
    render() {
        return (
            <NavItemli>
                <NavItem to={`${this.props.link}`}>{this.props.page}</NavItem>                
            </NavItemli>
        );
    }
};

export default SideNavItem;