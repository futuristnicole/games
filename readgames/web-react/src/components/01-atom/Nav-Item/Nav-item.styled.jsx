import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

import {  ColorWhite, BrandRed } from '../../00-base/colors.styles';

export const NavItem = styled(NavLink)`
    color: ${ColorWhite};
    text-decoration: none;
    // text-transform: uppercase;
    display: block;
    padding: 1.5rem 3rem;
    &:hover{
        color: ${BrandRed};
    }
    &:active {
        color: ${BrandRed};
    }
`;
export const NavItemli = styled.li`
    // color: ${ColorWhite};
    text-decoration: none;
    // text-transform: uppercase;
    // display: block;
    // padding: 1.5rem 3rem;
    // &:hover,
    // &:active {
    //     color: ${BrandRed};

    // }
`;