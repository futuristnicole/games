import React  from 'react';

import {  Point } from './IconBox.styled';


const IconBox = ({ point, id }) => (
    <div> <br />
        <Point>{point}</Point>   
    </div>  
);

export default IconBox;