import styled from 'styled-components';
import {  ColorBlack, ColorWhite, BrandRed_light } from '../../00-base/colors.styles';
import { BreakS } from '../../00-base/breakpoints';


// var rgba1 = "50, 20, 20, .5";
// var rgba2 = "85, 55, 55, .5";

export const CardGrid = styled.div`
    display: grid;
    grid-template-columns:  1fr 2fr .1fr;
    grid-gap: 20px;
    margin: auto;
    padding: 1rem;
    background-color: ${BrandRed_light}  ;
    border-radius: 2rem;
    grid-auto-rows: minmax(7rem, auto);
    height: 100%;
    width: 100%;
    @media screen and (min-width: ${BreakS}) {
        grid-template-columns: 10rem 1fr 4rem;
    }
`;

export const CardIMG = styled.div`
    background-image: url('../img/HomePage/Hero.jpg');
    
    background-size: cover;
    background-position: center;
    position: relative;
    height: 90%;
    width: 100%;
    border-radius: 50%;

`;

export const Title = styled.h3`
    // display: inline-block;
    // color: ${ColorWhite};
    // letter-spacing: .4rem;
    transition: all .2s;
    font-size: 2em;
`;

export const Headline = styled.p`
    // display: inline-block;
    color: ${ColorBlack};
    // letter-spacing: .4em;
    transition: all .2s;
    // font-size: 2.3em;
`;
export const Count = styled.p`
    // display: inline-block;
    // color: ${ColorWhite};
    // letter-spacing: .4em;
    transition: all .2s;
    // font-size: 2.3em;
    text-align: right;

    
`;