import React, { Component } from 'react'
// import ReactDOM from 'react-dom';
import NavItem from '../../01-atom/Nav-Item/Nav-item'
// import NavItem from '../Nav-item';

class TopNav extends Component {
    render() {
        return (
            <>
                <NavItem  link='/wordwalls' page='Word Walls' />
                <NavItem  link='/languagedisorder' page='Language Disorder' />
                <NavItem  link='/about' page='About' />
                <NavItem  link='/contact' page='Contact' />   
            </>
        )
    }
};

export default TopNav;