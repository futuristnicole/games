import React from 'react'
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom'

import Header from '../03-organisms/Header/Header'
import HomePage from '../05-pages/HomePage/HomePage'
import WallWidget from '../06-routes/WallWidget'

export default function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/addWall" component={WallWidget} />
      </Switch>
    </Router>
  )
}
