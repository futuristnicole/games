import {useState} from 'react';
function useToggle(initiaVal = false) {
    // call useState, "reserve piece of state"
    const [state, setState] = useState(initiaVal);
    const toggle =() => {
        setState(!state);
    };
    // return piece of state AND a funtion to toggle it
    return [state, toggle];
}

export default useToggle;