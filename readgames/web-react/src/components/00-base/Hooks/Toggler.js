import React from 'react';
import useToggle from './useToggle';

function Toggler() {
    const [isHappy, toggleIsHappy] = useToggle(true);
    return (
        <div>
            <h1 onClick={toggleIsHappy}>{isHappy ?  <>😊  <div className="box"></div> < /> : "😢"}</h1>
        </div>
    );
}

export default Toggler;