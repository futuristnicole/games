import React from 'react';

import TEAM_DATA from '../../Data/TeamData';
// import CollectionPreview from '../../02-molecules/';

import { Section, Title } from './'
import { Grid3 } from '../../04-templates/sections/Grid3';



class Team extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collections: TEAM_DATA
          };
        }
        render() {
            const { collections } = this.state;
            return (
              <Section >
                <Title>Word Walls</Title>
                <Grid3>
                    {collections.map(({ id, ...otherCollectionProps }) => (
                    <CollectionPreview key={id} {...otherCollectionProps} />
                    ))}
                </Grid3>
              </Section>
            );
          }
        }

export default Team;