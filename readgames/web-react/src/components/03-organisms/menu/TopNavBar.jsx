import React, { Component } from 'react';

import ToggleModalTopNav from '../../01-atom/ToggleModalTopNav/ToggleModalTopNav';
import TopNav from '../../02-molecules/TopNav/TopNav'
import { LogoText } from '../../01-atom/Logo/LogoText'

import { Topbar, NavTopul, Hide, LogoBox } from './TopNavBar.styled';

class TopNavBar extends Component {
    render() {
        return (
            <Topbar>
                <LogoBox to="/">
                    <LogoText boot />
                </LogoBox>
                <nav>
                    <NavTopul>
                        <TopNav />
                    </NavTopul>
                </nav>
                <Hide>
                    <ToggleModalTopNav/>
                </Hide>
            </Topbar>
        );
    }
};

export default TopNavBar;