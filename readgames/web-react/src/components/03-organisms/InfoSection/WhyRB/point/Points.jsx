import React, { Component, Fragment } from 'react';
import IconBox from '../../../02-molecules/IconBox/IconBox';


class Points extends Component {
    constructor() {
        super();

        this.state = {
            sections: [
                {
                    point: 'Improve social life with easier use of social media, texting and email',
                    id: 1
                },
                {
                    point: 'Increase education and employment opportunity',
                    id: 2
                },
                {
                    point: 'Take advantage MOOCs, traditional classes, and learning games that include reading',
                    id: 3
                }
            ]
        };
    }

    render() {
        return (
            <Fragment>
                {this.state.sections.map(({ point, id }) => (
                    <IconBox key={id} point={point} />
                ))}
              
            </Fragment>
        );
    }
};

export default Points;