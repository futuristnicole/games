import styled from 'styled-components';
import { BreakS, BreakM } from '../../00-base/breakpoints';
import { BrandRed } from '../../00-base/colors.styles'

export const NonProfitSection = styled.div`
    padding-top: 5rem;
    padding-bottom: 5rem;
    background-color: ${BrandRed};
    // margin-top: -6rem;
    clip-path: polygon(0 0, 100% 10%, 100% 100%, 0 90%); }
`;
export const Text = styled.div`
    font-size: 2.5em;
    color: white;
`;


export const ImgContainer = styled.img`
    background-size: cover;
    // width: 60%; 
    height: 100px;  
    @media screen and (min-width: ${BreakS}) {
        height: 200px; 
    }
    @media screen and (min-width: ${BreakM}) {
       width: 100%;
       height: auto;  
    }
  
`;