import styled from 'styled-components';
// import { BreakS, BreakM } from '../../00-base/breakpoints';
import { BrandRed, ColorWhite } from '../../00-base/colors.styles'

export const ResearchSection = styled.div`
    // padding-top: 5rem;
    // padding-bottom: 5rem;
    // background-color: ${BrandRed};
    // margin-top: -6rem;
    // clip-path: polygon(0 0, 100% 10%, 100% 100%, 0 90%); }
`;
export const CenterText = styled.p`
    text-align: center;
    padding: 4em;
    // padding-bottom: 3em;
    color: ${ColorWhite};
`;
export const Title = styled.h2`
    padding-top: 5rem;
    
    // text-align: center;
    font-weight: normal;
            
`;
export const Text = styled.p`
    // font-size: 2.5em;
    // color: white;
    padding-bottom: 2em;
`;

var rgba1 = "236, 20, 29, .7";
var rgba2 = "236, 20, 29, .4";

export const BackgroundImg = styled.div`
    background-image: -webkit-linear-gradient(bottom right, rgba(${rgba1}), rgba(${rgba2})), url('../img/AboutPage/ReseachBackground.jpg');
    background-image: -o-linear-gradient(bottom right, rgba(${rgba1}), rgba(${rgba2})), url('../img/AboutPage/ReseachBackground.jpg') ;
    background-image: linear-gradient(to top left, rgba(${rgba1}), rgba(${rgba2})), url('../img/AboutPage/ReseachBackground.jpg')  ;
    background-size: cover;
    background-position: center;
    position: relative;
    clip-path: polygon(0 10%, 100% 0, 100% 90%, 0 100%); 

`;