import React from 'react';
// import { FlexContainer, FlexBox, } from '../../04-templates/sections/flex';
import { ResearchSection, Title, Text, BackgroundImg, CenterText } from './Hedgehog.styled';


const Hedgehog = () => {
    return (
        <ResearchSection>
            <BackgroundImg>
                <CenterText>
                    <Title>Method</Title>
                    <Text>If someone cannot learn to read, write, or spell in any language, we want to know why.  So that find a way to help them learn.  Then we apply what we learned to expanding the help we offer to all.  </Text>
                </CenterText>  
            </BackgroundImg>
        </ResearchSection>
    );
};

export default Hedgehog;