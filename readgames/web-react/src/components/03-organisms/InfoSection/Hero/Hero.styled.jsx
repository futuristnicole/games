import styled, { css } from 'styled-components';
import { ColorWhite} from '../../00-base/colors.styles'

var rgba1 = "255, 180, 180, .5";
var rgba2 = "255, 255, 255, .5";

export const BackgroundHero = styled.div`
    height: 75vh;
    // background-image: url('../img/HomePage/Hero.jpg');
    background-image: -webkit-linear-gradient(bottom right, rgba(${rgba1}), rgba(${rgba2})), url('../img/HomePage/Hero.jpg');
    background-image: -o-linear-gradient(bottom right, rgba(${rgba1}), rgba(${rgba2})), url('../img/HomePage/Hero.jpg') ;
    background-image: linear-gradient(to top left, rgba(${rgba1}), rgba(${rgba2})), url('../img/HomePage/Hero.jpg')  ;
    background-size: cover;
    background-position: center;
    position: relative;
    clip-path: polygon(0 0,100% 0,100% 100%,0 90%);
`;

var rgba3 = "25, 18, 18, .7";
var rgba4 = "25, 25, 25, .6";

export const BackgroundVision = styled.div`
    height: 65vh;
    background-image: -webkit-linear-gradient(bottom right, rgba(${rgba3}), rgba(${rgba4})), url('../img/AboutPage/Vision.jpg');
    background-image: -o-linear-gradient(bottom right, rgba(${rgba3}), rgba(${rgba4})), url('../img/AboutPage/Vision.jpg') ;
    background-image: linear-gradient(to top left, rgba(${rgba3}), rgba(${rgba4})), url('../img/AboutPage/Vision.jpg')  ;
    background-size: cover;
    background-position: top;
    position: relative;
    clip-path: polygon(0 0,100% 0,100% 100%,0 90%);
`;

export const HeroCenterText = styled.div`
    position: absolute;
    top: 40%;
    left: 50%;
    transform: translate(-50%, -50%); }
`;

export const HeroText = css`
    display: block;
    text-align: center;
`;

export const HeroTextMain = styled.span`
    ${HeroText}
    font-size: 2.3em;
`;

export const HeroTextSub = styled.span`
    ${HeroText}
    font-size: .7em;
`;

export const VisionTextMain = styled.span`
    ${HeroText}
    color: ${ColorWhite};
    font-size: 1.8em;
`;

export const VisionTextSub = styled.span`
    ${HeroText}
    color: ${ColorWhite};
    font-size: .6em;
`;
