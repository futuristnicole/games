import React from 'react';
import { NavLink } from 'react-router-dom';

const HomePage = () => (
    <div className=''>
        <NavLink to='/'>HomePage</NavLink>
        <NavLink to='/addwall'>Add Wall</NavLink>
    </div>
);
export default HomePage;