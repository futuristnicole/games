import styled from 'styled-components';
import { BrandRed, BrandBlack, ColorWhite } from '../../00-base/colors.styles';

export const Legal = styled.div`
    font-size: 1.2em;
    text-align: center;
    padding: 1.5em;
    color: ${ColorWhite};
    background-color: ${BrandBlack};
    border-top: 3px solid ${BrandRed};
`;