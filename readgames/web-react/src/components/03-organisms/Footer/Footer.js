import React, { Component } from 'react';
import {Legal} from './Footer.styled'
import { LogoText } from '../../01-atom/Logo/LogoText'

class Footer extends Component {
    render() {
        return (
            <Legal>
                &copy; 2020 by <LogoText boot /> Inc. All right reserved. 
            </Legal>  
        );
    }
};

export default Footer;